# Linux-specific builder configurations and build commands

## Base images

### CentOS
#
# CentOS 7 is the primary deployment platform. This gains us maximum
# compatibility with many Linux distros while also providing easy ways to get
# newer compilers.

.centos7:
    image: "kitware/cmb:ci-superbuild-centos7-20240312"

    variables:
        LD_LIBRARY_PATH: /root/misc/root/smtk-deps/lib
        GIT_SUBMODULE_STRATEGY: none
        LAUNCHER: "scl enable devtoolset-10 rh-git227 --"
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

.centos7_modelbuilder:
    extends: .centos7

    variables:
        CMAKE_CONFIGURATION: centos7_modelbuilder

.centos7_docker_modelbuilder:
    extends: .centos7

    variables:
        CMAKE_CONFIGURATION: centos7_docker_modelbuilder

.centos7_vtk:
    extends: .centos7

    variables:
        CMAKE_CONFIGURATION: centos7_vtk

.centos7_aeva:
    extends: .centos7
    variables:
        CMAKE_CONFIGURATION: centos7_aeva

.centos7_ace3p:
    extends: .centos7
    variables:
        CMAKE_CONFIGURATION: centos7_ace3p

.centos7_truchas:
    extends: .centos7
    variables:
        CMAKE_CONFIGURATION: centos7_truchas

.centos7_cmb2d:
    extends: .centos7
    variables:
        CMAKE_CONFIGURATION: centos7_cmb2d

.centos7_windtunnel:
    extends: .centos7
    variables:
        CMAKE_CONFIGURATION: centos7_windtunnel

## Tags

.linux_builder_tags:
    tags:
        - build
        - cmb
        - docker
        - linux-x86_64

.linux_test_tags:
    tags:
        - cmb
        - docker
        - linux-x86_64
        - x11

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH
    - cmake --version
    - ninja --version
    - "git submodule update --init --recursive || :"
    - git submodule foreach --recursive cmake -P "$PWD/.gitlab/ci/fetch_submodule.cmake"
    - git submodule sync --recursive
    - git submodule update --init --recursive
    # Remove `freetype-devel` from the image; it confuses our `fontconfig`
    # builds. Remove it here to avoid rebuilding the image in the face of
    # CentOS 7's EOL.
    # Also remove `libpng-devel` to avoid building Qt against it.
    - yum -y remove freetype-devel libpng-devel

.cmake_build_linux:
    stage: build

    script:
        - *before_script_linux
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake"
        - sccache --show-stats

    interruptible: true

.cmake_test_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"
    interruptible: true
