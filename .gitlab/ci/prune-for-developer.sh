#!/bin/bash

set -e

# For each project to remove, clean its install artifacts.
for project in $PROJECTS_TO_CLEAN; do
    manifest="build/superbuild/$project/build/install_manifest.txt"
    sed -e "s,^$PREFIX/,," "$manifest" | tr -d $'\r' | \
        xargs \
            "--delimiter=\\n" \
            --max-args=40 \
            rm --force --verbose --dir
done

# Remove all pther build artifacts.
rm -rf build/superbuild
rm -rf build/cpack
rm -rf build/Testing
rm -rf build/CTest*
rm -rf build/DartConfiguration.tcl

# Remove empty directories.
find build -type d -empty -delete -print

# Create a tarball of the remaining build artifacts.
date="$( date "+%Y%m%d" )"
readonly date
readonly tarball="ci-$TARGET_PROJECT-ci-developer-$date-$CI_COMMIT_SHA-$PLATFORM.tar.gz"
tar czf "$tarball" build/

# Clean out the archived tree.
rm -rf build

# Move the tarball back to only make it visible for upload.
mkdir build
mv -v "$tarball" build/
