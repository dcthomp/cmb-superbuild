include("${CMAKE_CURRENT_LIST_DIR}/gitlab_ci.cmake")

# Read the files from the build directory.
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")

# Pick up from where the configure left off.
ctest_start(APPEND)

include(ProcessorCount)
ProcessorCount(nproc)

# Make files wanted by SMTK's testing.
file(MAKE_DIRECTORY "${CTEST_BINARY_DIRECTORY}/superbuild/smtk/build/Testing")
file(MAKE_DIRECTORY "${CTEST_BINARY_DIRECTORY}/superbuild/smtk/build/Testing/Temporary")

set(test_exclusions
  # Python3 support is missing in the ACE3P workflow.
  # https://gitlab.kitware.com/cmb/simulation-workflows/-/issues/2
  "TestSimExportOmega3P_01Py"
)

if ("$ENV{CI_JOB_NAME}" MATCHES "centos7")
  list(APPEND test_exclusions
    # Temporary exclusion of run-time arc tests and worklet UI for aeva
    "^TestRuntimeJSON$"
    "^TestMarkupResource$"
    # OpenGL shader-using tests.
    "display(AuxiliaryGeometry|MultiBlockModel-test2D|ModelToMesh-simple)"
    "UnitTestRead(2dm|3dm|Obj|Pts|Stl|Vtp|Xyz)"
    "DeleteSmtkCell"
    "discreteImport2dmTest"
    "^import-"
    "^pv\\."
    "RenderMesh"
    "unitQtComponentItem"
    "^UnitTestDoubleClickButton$")
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "windows")
  list(APPEND test_exclusions
    # Zoomed in somehow?
    "displayAuxiliaryGeometry"
    # Rendering changes.
    "displayModelToMesh-simple"
    # EGL context creation fails.
    "^pv\\."
    # Unknown SEGFAULT.
    "^ImportMultipleFiles$"
    # Heap corruption; valgrind does not find it.
    "^snapPointsToSurfacePy$"
    "^TestInterpolateAnnotatedValues$"
    "^TestSamplePointsOnSurface$"
    "^TestSelectionFootprint$"
    "^TestSnapPointsToSurface$"
    "^TestTransform$"
    "^TestTransformOp$")

  if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "ace3p")
    list(APPEND test_exclusions
      # Error importing vtk with ParaView 5.9 branch
      "^openSMTKFileInParaViewPy$")
  endif ()
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "macos")
  list(APPEND test_exclusions
    # Rendering changes.
    "displayModelToMesh-simple"

    # Failing only on macOS. Needs investigation.
    "^pv\\.MeshSelection$"
    "^pv\\.WorkletPanel$"
    "^pv\\.OpenExodusFile$"

    # UI path changes.
    "^pv\\.AgentPorts$"
  )
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "macos-x86_64")
  list(APPEND test_exclusions
    # Segfaults that need investigated.
    "^testImportPythonOperation$"
    "^TestManagersAccess$")
endif ()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()

ctest_test(
  PARALLEL_LEVEL "${nproc}"
  RETURN_VALUE test_result
  EXCLUDE "${test_exclusions}"
  OUTPUT_JUNIT "${CTEST_BINARY_DIRECTORY}/junit.xml")
ctest_submit(PARTS Test)

include("${CMAKE_CURRENT_LIST_DIR}/ctest_annotation.cmake")
if (DEFINED build_id)
  ctest_annotation_report("${CTEST_BINARY_DIRECTORY}/annotations.json"
    "All Tests"     "https://open.cdash.org/viewTest.php?buildid=${build_id}"
    "Test Failures" "https://open.cdash.org/viewTest.php?onlyfailed&buildid=${build_id}"
    "Tests Not Run" "https://open.cdash.org/viewTest.php?onlynotrun&buildid=${build_id}"
    "Test Passes"   "https://open.cdash.org/viewTest.php?onlypassed&buildid=${build_id}"
  )
endif ()

# upload generated packages to CDash
file(GLOB files
  "${CTEST_BINARY_DIRECTORY}/*.dmg"
  "${CTEST_BINARY_DIRECTORY}/*.exe"
  "${CTEST_BINARY_DIRECTORY}/*.tar.*"
  "${CTEST_BINARY_DIRECTORY}/*.zip")
if (files)
  ctest_upload(FILES ${files})
  ctest_submit(PARTS Upload)
endif()

if (test_result)
  message(FATAL_ERROR
    "Failed to test")
endif ()
