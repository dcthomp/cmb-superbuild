include("${CMAKE_CURRENT_LIST_DIR}/gitlab_ci.cmake")

# Read the files from the build directory.
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")

# Pick up from where the configure left off.
ctest_start(APPEND)

ctest_build(
  NUMBER_ERRORS num_errors
  NUMBER_WARNINGS num_warnings
  RETURN_VALUE build_result)
ctest_submit(PARTS Build)

file(GLOB logs
  "${CTEST_BINARY_DIRECTORY}/superbuild/*/stamp/*-build-*.log"
  "${CTEST_BINARY_DIRECTORY}/superbuild/*/stamp/*-install-*.log")
if (logs)
  list(APPEND CTEST_NOTES_FILES ${logs})
  ctest_submit(PARTS Notes)
endif ()

include("${CMAKE_CURRENT_LIST_DIR}/ctest_annotation.cmake")
if (DEFINED build_id)
  ctest_annotation_report("${CTEST_BINARY_DIRECTORY}/annotations.json"
    "Build Errors (${num_errors})" "https://open.cdash.org/viewBuildError.php?buildid=${build_id}"
    "Build Warnings (${num_warnings})" "https://open.cdash.org/viewBuildError.php?type=1&buildid=${build_id}"
  )
endif ()

if (build_result)
  message(FATAL_ERROR
    "Failed to build")
endif ()
