superbuild_add_project(moab
  DEPENDS cgm eigen netcdf zlib
  CMAKE_ARGS
    # Set link path on OSX
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DENABLE_IMESH:BOOL=ON
    -DENABLE_CGM:BOOL=ON
    -DENABLE_NETCDF:BOOL=ON
    -DENABLE_IREL:BOOL=ON
    -DENABLE_HDF5:BOOL=OFF
    -DENABLE_PNETCDF:BOOL=OFF
    -DENABLE_MPI:BOOL=OFF
    -DENABLE_TESTING:BOOL=OFF
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>)

superbuild_apply_patch(moab disable-fortran
"Disable use of fortran")

# By default, linux and os x cmake looks in <INSTALL_DIR>/lib/cmake for
# things. On windows, it does not. So, we set MOAB_DIR to point to the
# location of MOABConfig.cmake for everyone.
superbuild_add_extra_cmake_args(
  -DMOAB_DIR:PATH=<INSTALL_DIR>/lib/cmake/MOAB)
