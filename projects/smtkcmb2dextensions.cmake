set(smtkcmb2dextensions_extra_cmake_args)

set(smtkcmb2dextensions_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtkcmb2dextensions_rpaths
    "${qt5_rpath}")
endif ()

string(REPLACE ";" "${_superbuild_list_separator}"
  smtkcmb2dextensions_rpaths
  "${smtkcmb2dextensions_rpaths}")

set(smtkcmb2dextensions_enable_by_default OFF)
if ("${SUPERBUILD_PACKAGE_MODE}" STREQUAL "cmb2d")
  set(smtkcmb2dextensions_enable_by_default ON)
endif()

superbuild_add_project(smtkcmb2dextensions
  DEVELOPER_MODE
  DEBUGGABLE
  DEPENDS boost cxx11 paraview python3 qt5 smtk smtkxmsmesher
  CMAKE_ARGS
    ${smtkcmb2dextensions_extra_cmake_args}
    -DBUILD_EXAMPLES:BOOL=OFF
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_RPATH:STRING=${smtkcmb2dextensions_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DPYBIND11_INSTALL:BOOL=${pybind11_enabled}
    -DCMAKE_INSTALL_CMBWORKFLOWDIR:PATH=<INSTALL_DIR>/share/cmb/workflows
    -DENABLE_PLUGIN_BY_DEFAULT:BOOL=${smtkcmb2dextensions_enable_by_default}
  )

superbuild_declare_paraview_xml_files(smtkcmb2dextensions
  FILE_NAMES "smtk.cmb-2d.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
