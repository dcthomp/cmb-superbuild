set(moab_shared_settings
  -DBUILD_SHARED_LIBS:BOOL=OFF
  -DCMAKE_CXX_VISIBILITY_PRESET:STRING=hidden)
if (NOT WIN32)
  set(moab_shared_settings
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib
    -DCMAKE_CXX_VISIBILITY_PRESET:STRING=default)
endif ()

if (APPLE)
  set(moab_rpath "@loader_path/")
elseif (UNIX)
  set(moab_rpath "${superbuild_install_location}/lib")
endif ()

superbuild_add_project(moab
  DEPENDS eigen hdf5 netcdf
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -Wno-dev
    ${moab_shared_settings}
    -DCMAKE_VISIBILITY_INLINES_HIDDEN:BOOL=ON
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_RPATH:STRING=${moab_rpath}
    -DENABLE_BLASLAPACK:BOOL=OFF
    -DMOAB_HAVE_EIGEN:BOOL=ON
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=ON
    -DENABLE_HDF5:BOOL=ON # also required to get hdf5 support enabled
    -DMOAB_USE_NETCDF:BOOL=ON
    -DENABLE_NETCDF:BOOL=ON # also required to get ncdf/exodus enabled
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>
    -DMOAB_BUILD_HEXMODOPS=OFF # app doesn't pass verification on install, centos
    -DMOAB_BUILD_MBCONVERT:BOOL=OFF
    -DMOAB_BUILD_MBCOUPLER:BOOL=OFF
    -DMOAB_BUILD_MBDEPTH:BOOL=OFF
    -DMOAB_BUILD_MBGSETS:BOOL=OFF
    -DMOAB_BUILD_MBHONODES:BOOL=OFF
    -DMOAB_BUILD_MBMEM:BOOL=OFF
    -DMOAB_BUILD_MBPART:BOOL=OFF
    -DMOAB_BUILD_MBQUALITY:BOOL=OFF
    -DMOAB_BUILD_MBSIZE:BOOL=OFF
    -DMOAB_BUILD_MBSKIN:BOOL=OFF
    -DMOAB_BUILD_MBSLAVEPART:BOOL=OFF
    -DMOAB_BUILD_MBSURFPLOT:BOOL=OFF
    -DMOAB_BUILD_MBTAGPROP:BOOL=OFF
    -DMOAB_BUILD_MBTEMPEST:BOOL=OFF
    -DMOAB_BUILD_MBUMR:BOOL=OFF
    -DMOAB_BUILD_SPHEREDECOMP:BOOL=OFF
    -DENABLE_TESTING:BOOL=OFF)

superbuild_apply_patch(moab disable-fortran
  "Disable use of fortran")

superbuild_apply_patch(moab export-include-dir
  "Set MOAB and iMesh targets to export their installed include directories")

superbuild_apply_patch(moab find-hdf5-default-path
  "When using CMake to look for HDF5, use the default path")

superbuild_apply_patch(moab add-alternate-name-for-triangle
  "Add ExodusII standard label TRIANGLE for triangle elements")

superbuild_apply_patch(moab ext-deps
  "Re-find external dependencies when finding MOAB")

superbuild_apply_patch(moab no-mtune
  "Disable `-mtune=native` flag")

# macOS on arm64 makes copies of classes it can completely see and the `typeid`
# mechanism ends up being confused. This is used inside of MOAB to determine
# what kind of interface to generate, so the calling library needs to agree on
# the provenance of types with MOAB when it is built.
superbuild_apply_patch(moab iface-home-library-anchoring
  "Hide implementations of interface ctor and dtor")

# By default, linux and os x cmake looks in <INSTALL_DIR>/lib/cmake for
# things. On windows, it does not. So, we set MOAB_DIR to point to the
# location of MOABConfig.cmake for everyone.
superbuild_add_extra_cmake_args(
  -DMOAB_DIR:PATH=<INSTALL_DIR>/lib/cmake/MOAB)
