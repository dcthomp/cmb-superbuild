# Capstone is a private program to which Kitware developers have access. We
# provide logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
find_package(CreateMG REQUIRED)

set(capstone_pythonpath ${CreateMG_DIR}/bin)

if (APPLE)
  set(capstone_pythonpath ${CreateMG_DIR}/../Capstone.app/Contents/MacOS)
endif ()
