message(STATUS "Configuring modulebuild+ACE3P package")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_REQUIRED_PROJECTS" "smtkace3p;cmb;smtk")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_EXCLUDE_PROJECTS" "")

include(SuperbuildVersionMacros)
superbuild_set_version_variables(smtkace3p "1.0.0" "smtkace3p-version.cmake" "version.txt")

set(package_extra_projects
  smtkace3p
  openssl
  cumulus
)
list(APPEND superbuild_extra_package_projects "${package_extra_projects}")

list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPQProjectPlugin
  smtkPVMeshExtPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
set_property(GLOBAL APPEND PROPERTY cmb_extra_plugins ace3p-extensions)
set_property(GLOBAL APPEND PROPERTY cmb_extra_dependencies smtkace3p)

# Configure Options
set(ENABLE_smtkace3p ON)
set(ENABLE_cmb ON)
set(ENABLE_cmbworkflows OFF)
set(ENABLE_smtkresourcemanagerstate OFF)

set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF)

set_property(GLOBAL APPEND PROPERTY cmb_extra_package_cmake_arguments
  -Dcmb_enable_multiservers:BOOL=ON
  -Dcmb_enable_objectpicking:BOOL=ON)

# Turn on OpenSSL
set(_superbuild_enable_openssl ON)
set(ENABLE_openssl ON)

if (DEVELOPER_MODE_smtk)
  message(FATAL_ERROR "Cannot build in ace3p packaging mode if DEVELOPER_MODE_smtk is ON")
endif ()

include(CMBBundleMacros)
cmb_generate_package_suffix(${SUPERBUILD_PACKAGE_MODE})
cmb_generate_package_bundle(${SUPERBUILD_PACKAGE_MODE}
  PACKAGE_NAME "modelbuilder-${SUPERBUILD_PACKAGE_MODE}"
  DESCRIPTION "CMB + ACE3P"
  PACKAGE_VERSION smtkace3p
  HAS_WORKFLOWS
  EXCLUDE_VERSION
  )
